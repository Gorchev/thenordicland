---
layout: default
title: Blog - Thenordicland - Deni Gorchev
description: Hi, I'm Deni and I provide freelance web development, design and brand identity services. I can help you with your next great project, whether is small or big, to succeed on-line.
keywords: web design, design, user experience, web development, brand identity, brand, ruby on rails, wordpress, cms, content management system, logo design, business cards, letterheads, psd to html, wireframes, london, paris, warsaw, europe, america, thenordicland, mladen gorchev, deni gorchev
---

<div class="blog_page padding_left">

  {% include header.html %}

  <!-- Left Area -->
  <div class="left_area">
    <a href="http://thenordicland.io" class="logo">
      <img src="{{relative}}images/logo_white.png" class="whole" tilte="" alt="Thenordicland Logo" />
      <img src="{{relative}}images/logo_half_green.png" class="half" tilte="" alt="Thenordicland Logo" />
    </a>

    <div class="side_menu">
      <h2>Blog</h2>
      <!--<ul>
        <li class="current"><a href="">Brand Identity</a></li>
        <li><a href="">Design &amp; User Experience</a></li>
        <li><a href="">Web Development &amp; CMS</a></li>
      </ul>-->
    </div>

    <div class="ie_opacity"></div>
  </div>
  <!-- Left Area End -->

  <!-- Content -->
  <div class="content">

    <div class="padding_top right_content">
      <h1>Happy <?php echo date('l'); ?>!</h1>
      <br />
      <div class="blog_articles">
        {% for resources in site.categories.resources %}
        <div class="article">
          <div class="thumbnail"><img src="{{ resources.image }}" /></div>
          <h2><a href="{{ resources.url | prepend: site.baseurl }}">{{ resources.title }}</a> <span>Written on {{ resources.date | date: "%b %-d, %Y" }} by {{resources.from}}</span></h2>
          <p>{{ resources.content | strip_html | truncatewords:75}} <a href="{{ resources.url | prepend: site.baseurl }}">Read more -></a></p>
        </div>
        {% endfor %}
      </div>
    </div>

  </div>
  <!-- Content End -->

  {% include footer.html %}

</div>
