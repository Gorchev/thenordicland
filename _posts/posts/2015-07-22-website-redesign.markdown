---
layout: post
title:  "When you should consider redesign of your website"
date:   2015-07-22 11:16:00
categories: posts
image: http://thenordicland.io/uploads/redesign.jpg
from: Marcjanna Paszak
---


Your website is your primary marketing tool and the best ‘sales person’ in the company. It exists to represent your business and build your customer base, so the data should show that it is hitting it’s targets.

The biggest mistake is to invest a lot of money in a new website but do so without measurable goals and objectives. A lot of companies like new design but do not know if their website is getting the results they need. It is important to begin with end in mind when you redesign your website. 

We could analyse your website in many different ways, but in reality, it will always come down to producing business results- attracting new customers and  increasing conversion. In todays world this can be achieved under three conditions: 

-being prepared for visitors on mobile devices
-making sure your company can be found in search engines
-keeping the user on the page through supplying valuable content 

If you then have the tools in place to accurately measure your website’s results you have everything what you need.

Examining your site's conversion rates-visitor-to-lead and lead-to-customer can provide you with a clear idea of what needs to be done.

So what are the questions you should ask yourself to check if your website is doing a good job for your business? There are few basic things to consider:


##1. Is your website accessible via smartphones and tablets?

Increase of mobile usage for internet browsing and online shopping is so obvious that we don’t even have to talk about it anymore and all companies know that they are missing out on sales opportunities if their website is not mobile friendly yet.
But there is one more side to responsive design that has to be taken under account- SEO! 
On April 21st, 2015 Google released its latest algorithm that will “reward” those sites that are mobile optimised, and “punish” those that are not. The algorithm nicknamed “mobilegeddon” will affect a lot of websites, and most of those will be of course small businesses.

You can perform a [‘Mobile-Friendly Test’](https://www.google.com/webmasters/tools/mobile-friendly/){:target="_blank"} with Google and check how your website looks on mobile devices.

Check out [Thenordicland](http://thenordicland.io/design_and_ux.html) examples of [mobile friendly designs on Behance](https://www.behance.net/gallery/22758379/Turek-Responsive-HTML-Template){:target="_blank"}

##2. Is your website optimised for search engines?
                 
Content is the celebrity in the world of search engines. Link building campaigns might work in a short term but to gain real visitor loyalty and traffic retention you need optimised content that brings real value to the user. 
You will always be on the winning position if you write real quality pieces for humans not for search engines. If you come up with the content people want to share on Facebook, Twitter, Pinterest and other social networks this will bring natural traffic to your site.
A blog is a great way to create content on an ongoing basis, and to start to engage with your potential clients

##3. Is your Content Management System and Website Technology up to date?

The web world is always changing, and web technology that was cutting-edge a few years ago may be out-of-date today. If your website has been built on older technology you may find that your editing choices became limited. By having your website redesigned on a more contemporary, progressive platform you will be able to adapt with technology, maintain greater security and have more flexibility.
The key to your website success over time is to constantly improve the effectiveness of your conversion tools and this usually means your landing pages.
If you build a static website you might be very limited in your ability to quickly experiment and improve because you will need help from IT expert to perform even small changes on your site.
A system that lets you edit content and build landing pages without having to know coding is the best investment for your business.


##4. Is your website user friendly?

If you’re site visitors can’t find what they’re looking for, or it lasts too long, they’ll leave immediately and probably end up on the competitors site. There can be elements on your website that are confusing or not effective enough and you may not even realise that.
If that’s the case, you definitely need a redesign that is user-friendly and keeps your visitors on your page.
Functionality should be a paramount focus, that comes together with a great content and design.
If your site navigation is confusing or a visitor cannot find the most basic things such as contact information it can be highly discouraging and frustrating. It’s like being in a restaurant where you can’t understand the menu and a missing waiter.


##6. Does your Website Design represent your character?

The first impression really counts. Every good sales person knows that you only have a few seconds to get user's attention and to engage them. The quality of your website is a direct reflection on your business. It is your shortest sales pitch that can reach the broadest audience so it has to look contemporary and fresh. Also when the aesthetics of your website design is old, probably the user experience is also not the best.
To have an idea about the changing trends you can observe [how the most popular websites changed over the years]( http://tutorialzine.com/2015/03/how-your-favorite-websites-changed-over-the-years/){:target="_blank"}

##7. Are you socialising?

Make it easy for your visitors to connect with you and make it as simple as possible for readers to share everything what they do with others. 
Adding customisable social sharing and following buttons will help you to engage with more people and distribute your content through multiple platforms. You can do it easily using platforms like [AddThis](http://www.addthis.com/){:target="_blank"} or [ShareThis](http://www.sharethis.com/){:target="_blank"}. Both of them give you easy-to-use solutions with the added benefit of analytics to see how your content is being shared.


##8. Do you have all the functionality that you need?

If you decide you are missing some useful functionality like a blog, forum, e-commerce a redesign is what you need, because only in this way you can ensure that everything flows seamlessly throughout your website. 


##9. Or maybe you need to shake things up?

From time to time you just need a change or general cleaning and throwing things from the attic. If your website starts to look tired and old a little revamp is just what you need.

And it doesn’t mean that you have to start all over again!!!

You might think it will take a lot of time and cost a lot of money, but it doesn’t have to be this way.
A website redesign doesn’t always mean that you have to completely scrap your old website and start something new.  Sometimes simple changes like new layout, adding headlines, new graphics or re-writing pages can have a big impact on user experience.

The most important thing is to have plan how to test the results, so you know, if the changes had the intended effect and if it helped your website work better and achieve your marketing goals.

<div class="report">
<img src="/images/report.jpg" alt="Complimentary Website Evaluation Report" />
<p>Request our Complimentary Website Evaluation Report from <a href="http://thenordicland.io/">Thenordicland</a> and find out what we could do for your website!</p>

<a href="http://thenordicland.io/website_report.html" class="button purple big report">Complimentary Website Evaluation Report</a>
</div>


