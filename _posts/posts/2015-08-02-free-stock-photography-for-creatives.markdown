---
layout: post
title:  "Free Stock Photography for Creatives"
date:   2015-08-02 11:37:00
categories: posts
image: http://thenordicland.io/uploads/responsive.jpg
from: Marcjanna Paszak
og-image: http://thenordicland.io/uploads/free-stock-photos/og-image.jpg
---

Creative professionals of all kinds are on the hunt for pictures that do not look like a “typical stock photos” to avoid cliché in their design.

There are already quite a few websites offering free, good quality photography that is not overused and doesn’t look funny or cheesy. 

The quickest way is to use Stock Up that is for stock photography like Sky Scanner for flights booking. Stock Up is a search engine that now searches across 24 free stock photo websites: [Unsplash](https://unsplash.com/), [Life of Pix](http://www.lifeofpix.com/), [Jay Mantri](http://jaymantri.com/), [Startup Stock](http://startupstockphotos.com/), [Splitshire](http://www.splitshire.com/), [MMT](http://mmt.li/), [Public Domain Archive](http://publicdomainarchive.com/), [Refe](http://getrefe.tumblr.com/), [Magdeleine](http://magdeleine.co/), [Kaboom Pics](http://kaboompics.com/), [Good Stock](http://goodstock.photos/), [Skitter Photo](http://skitterphoto.com/), [New Old Stock](http://nos.twnsnd.co/), [Picography](http://skitterphoto.com/), [Cupcake](http://cupcake.nilssonlee.se/), [Snapwire Snaps](http://snapwiresnaps.tumblr.com/), [Free Nature Stock](http://freenaturestock.com/), [Free iMage Bank](http://www.freemagebank.com/), [Don’t Erase](http://georgeyanakiev.com/donterase/), [Barn Images](http://barnimages.com/), [Negative Space](http://negativespace.co/), [Libreshot](http://libreshot.com/), [Bucketlistly Photos]( http://photos.bucketlistly.com/) and [Good Free Photos](http://www.goodfreephotos.com/).

The websites have their own individual rules so for usage guidelines you need to refer to the original source, but StockUp works really well as a search tool. Each website is indexed every week, so there are always new photos to look at.

Most of the websites have the own search functionality or general categories like: food, architecture, fashion, landscapes so once you will find your favourite one you can always quickly find what your are looking for.

There are also many websites that are not listed on StockUp worth checking: [Pexels](http://www.pexels.com/), [Little Visuals](http://littlevisuals.co/), Stocksnap (https://stocksnap.io/)
[PicJumbo](https://picjumbo.com/), [Startup Stock Photos](http://startupstockphotos.com/) and [Fancy Crave](http://fancycrave.com/).

##How does it work? Why is it free?

Most of the websites offer photographs that are free from copyright restrictions or licensed under Creative Commons so you can copy, modify and distribute the images for commercial purposes. Some of them require attribution, so make sure you double check it before downloading the image!

Usually the services is free because the website has an upgrade option and you pay a subscription to access more photographs or it aims to attract potential new clients to a creative business that stands behind the project.

##Our favourites

[New Old Stock](http://nos.twnsnd.co/) collects interesting vintage pictures from the public archives that are free of known copyright restrictions. You can really find some rare, original stuff here!

![New Old Stock](http://thenordicland.io/uploads/free-stock-photos/site_1.jpg)

[Gratisography](https://goodstock.photos/) created by Ryan McGuire offers some original photos that have a characteristic style that Rayan describes himself as “whimsical

![Gratisography](http://thenordicland.io/uploads/free-stock-photos/site_2.jpg)

[Death to the Stock](http://deathtothestockphoto.com/about/) created by Allie and David who aim to fund the creative projects around the world through giving part of their premium membership profits! We like it!!! :)

![Death to the Stock](http://thenordicland.io/uploads/free-stock-photos/site_3.jpg)

If you know some cool website worth recommending post it in the comments below!




