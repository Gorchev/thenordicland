---
layout: post
title:  "Turek - Free Rsponsive HTML Template for your services"
date:   2015-07-23 12:30:00
categories: resources
image: http://thenordicland.io/uploads/parallax.jpg
from: Deni Gorchev
---

Turek is an HTML template which can be used for many purposes. Please feel free to [test](http://turek.rubythemes.co){:target="_blank"} and download. If you need any assistance and help to set it up, transfrom to WordPress, or support in general, please [get in touch with me](/hire_me.html).

<div class="report">
<img src="/images/gift.jpg" alt="Complimentary Website Evaluation Report" />
<p>Turek Responsive HTML Template.zip</p>

<a href="http://rubythemes.co/download/turek_responsive_html_template.zip" class="button purple big report">Download</a>
</div>



