---
layout: post
title:  "Super Simple Parallax Effect"
date:   2015-06-29 13:51:00
categories: resources
image: http://thenordicland.io/uploads/parallax.jpg
from: Deni Gorchev
---

The Parallax effect, which most of us have seen on many websites, can be achieved in many ways. However, I like the the simple ones... This example, which you will see below has not been created by me but from these guys <a href="http://parascroll.wwwtyro.net/" target="_blank">here</a>. I just want to put it here as a source which I can always refer to when I need it quickly. As well as, here is done with jQuery rather than pure JS.

The Parallax
---------
{% highlight javascript %}
function onLoad() {
	window.onscroll = function() {
	    var speed = 5.0;
	    $(".parallax").css("background-position-y", (window.pageYOffset / speed) + "px");
	}
}

onLoad();
{% endhighlight %}

Make sure you put the code above within 
{% highlight javascript %}
$(document).ready(function(){
	
});
{% endhighlight %}

In our example, we animate the "background-position" property on the "Y" axis, which means the animation will be vertical. However, we also can animate the "top", "margin-top", "padding-top" etc. The only thing is if we animate everything else except "background-position" we need to have a container that contains the element we want to animate and the animated element should have a "position: relative" or "position:absolute" in the css file.


