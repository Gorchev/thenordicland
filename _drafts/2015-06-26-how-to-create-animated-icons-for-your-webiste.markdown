---
layout: post
title:  "How to animate an icon for your website"
date:   2015-06-26 17:04:00
categories: tutorials animation
---

I've been always interested how people create these cool animations... Then sudenly it poped up that Adobe After Effects is the tool for that... I mean you can achieve very good animations with CSS3 but still some of the browsers like IE 9 doesn't support it fluently.

What we are going to do?
------------------------

![Animated rocket - Thenordicland](/tuts/anim-rocket/animated-rocket.gif)

In this tutorial I will show you how to animate an icon. I haven't done the icon by my self, I've bought the pack from <a href="http://www.shutterstock.com/pic-180717338/stock-vector-school-and-university-outline-icons.html?src=ruUK0_UGNystBBa3yPqBTg-1-14" target="_blank">here</a>

Step 1 - preparing the objects
------------------------------

First we need to know how the animation will look like. In our case I'm going to animate the smoke and the body of the rocket. That means we need to separate every element, that we are going to animate, into dfferent layers.

![Step 1 - Separate into different layers - Thenordicland](/tuts/anim-rocket/1.png)
![Step 1 - Separate into different layers - Thenordicland](/tuts/anim-rocket/2.png)

...then we save it as a PSD

![Step 1 - Save as PSD - Thenordicland](/tuts/anim-rocket/3.png)

Step 2 - working in After Effects
---------------------------------

After we have saved the icon as a PSD, we need to import it in After Effects - *File > Import > Files...*. Make sure that you select *Composition - Retain Layer Sizes*

![Step 2 - All files - Thenordicland](/tuts/anim-rocket/4.png)

then click on the composition icon on the left and you should see all the layers as below

![Step 2 - All files - Thenordicland](/tuts/anim-rocket/5.png)

Now we are ready to create the animation!

###Animating the smoke

In our case I'm going to animate the smoke first. I want to make it pulsing from time to time. To do so we need to work with the properties. 

We have three layers called "smoke" they are basically the lines under the rocket. We can start with the first one.

1. Open the properties of the layer - * Click on the arrow of the layer > click the arrow of "Transform"* then you can see all properties e.g. Anchor Point, Position, Scale etc.

![Step 2 - Working with keyframes - Thenordicland](/tuts/anim-rocket/6.png)

2. To make the smoke pulsing we are going to work only with the scale property. On the keyframes, which you can see on the right from the properties, we need to set the scale to "0%" on the first frame. Then we need to set the scale randomly on different frames e.g. in our case on frame "6" we have "71%"" then on frame "9" we have "105%" and so on... This will give the line diferent scale on different frames and thus we will animate the line.

3. Repeat the same step for the other two lines (smokes). 

![Step 2 - Working with keyframes - Thenordicland](/tuts/anim-rocket/7.png)

###Animating the body

Before we start with the body of the rocket, we first need to make sure that we connect the smoke with the body, so when we rotate or scale the body, the smoke will move with it. In this case, as you can see on the image below, I drag and drop the "Parent" line above the layer of the "body". In this way we make all smoke elements children of the "body".

![Step 2 - Animating the body - Thenordicland](/tuts/anim-rocket/8.png)

Next, we are going to animate the body just with the "Rotation" property. We will do the same steps as we did with the smoke (you can see the keyframes in source file).

Basically in the end you should have the same effect as the image below.

![Animated rocket - Thenordicland](/tuts/anim-rocket/animated-rocket.gif)

If the result is not the same, j


