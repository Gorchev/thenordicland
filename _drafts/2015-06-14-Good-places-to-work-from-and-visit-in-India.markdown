---
layout: post
title:  "Good places to work from and visit in India"
date:   2015-06-23 15:34:18
categories: traveling work
---
India is very big and as they say on the advert "Incredible". There are quiet many places to visit and discover, and definitely if you need to work while being there, you better check the places. 
I'm not gonna cover whole India, however just a small part of it. 

New Delhi - The Madpackers Hostel
---------
In general there are many places - hotstels, hotels, guest houses etc. where you can stay and enjoy your visit in Delhi. However what if I want something cheaper and at the same time good quality? Well in New Delhi, one place like this is <a href="http://www.hostelworld.com/hosteldetails.php/The-Madpackers-Hostel/New-Delhi/91989" target="_blank">"The Macpackers Hostel"</a>. We spent around £40 for 6 days to sleep in a dormitory with 6 beds. As you will see on their web site, they have very nice clean rooms and toilets, and what makes them "stand out" is the living room that makes the atmosphere relaxed. 

*They have fast internet (I mean you can really do some job there)*, air conditioning (if you go in Summer... 43 deg celsius... it is needed!), free breakfast... and many people from the west. It is also close to Hauz Khas metro station from where you can make good connections with other lines.

Leh, Ladakh, Northern India
---------------------------

Ladakh is one amazing place! It looks very similar to the Tibetan landscapes and is located in the Himalayas and Karakoram mountain ranges... exciting isn't it?

To access Ladakh there are 3 options:
1. On land - from Manali to Leh
2. By Plane from Delhi
3. On land - from Srinagar to Leh (I don't recommend).

The roads during the winter period are closed. The road from Srinagar opens around April - May, and the road from Manali opens mid June - July. For regular updates on the road you can check in here <a href="http://devilonwheels.com/manali-leh-highway-status-2015/">http://devilonwheels.com/manali-leh-highway-status-2015/</a> (there is also the road Srinagar - Leh).

In Leh if you really want to save money go to "Tsavo Guest House"... There is no address, however there is an <a href="allurespring05@yahoo.com">email</a> you can use. The consitions are not Great, however is very cheap and the people are amazing!

About internet... well in Leh is very hard to get good speed wi-fi, so better be sure that you won't need internet constantly. We managed to get some work done in the mornings  and in the afternoons from time to time. To get a 3G sim card there you must have 5 photos and printed passport and visa. 

For internet and good food you can visit <a href="http://www.tripadvisor.com/Restaurant_Review-g297625-d1197115-Reviews-Lamayuru_Restaurant-Leh_Ladakh_Jammu_and_Kashmir.html">"Lamayuru Restaurant"</a>


