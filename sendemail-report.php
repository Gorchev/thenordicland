<?php

if(isset($_POST['name']))
{
$name =$_POST['name'];
$email_name = $name;
}

if(isset($_POST['email']))
{
$email = $_POST['email'];
$email_email = $email;
}

if(isset($_POST['website']))
{
$website = $_POST['website'];
$email_website = $website;
}

if(isset($_POST['textarea1']))
{
$textarea1 =$_POST['textarea1'];
$email_textarea1 = $textarea1;
}

if(isset($_POST['textarea2']))
{
$textarea2 = $_POST['textarea2'];
$email_textarea2 = $textarea2;
}

if(isset($_POST['textarea2']))
{
$textarea3 = $_POST['textarea3'];
$email_textarea3 = $textarea3;
}

if(isset($_POST['textarea3']))
{
$textarea4 = $_POST['textarea4'];
$email_textarea4 = $textarea4;
}

if(isset($_POST['textarea5']))
{
$textarea5 = $_POST['textarea5'];
$email_textarea5 = $textarea5;
}

if(isset($_POST['textarea6']))
{
$textarea6 =$_POST['textarea6'];
$email_textarea6 = $textarea6;
}

if(isset($_POST['textarea7']))
{
$textarea7 =$_POST['textarea7'];
$email_textarea7 = $textarea7;
}

if(isset($_POST['textarea8']))
{
$textarea8 = $_POST['textarea8'];
$email_textarea8 = $textarea8;
}

if(isset($_POST['textarea9']))
{
$textarea9 = $_POST['textarea9'];
$email_textarea9 = $textarea9;
}

if(isset($_POST['textarea10']))
{
$textarea10 = $_POST['textarea10'];
$email_textarea10 = $textarea10;
}

if(isset($_POST['textarea11']))
{
$textarea11 = $_POST['textarea11'];
$email_textarea11 = $textarea11;
}

if(isset($_POST['textarea12']))
{
$textarea12 =$_POST['textarea12'];
$email_textarea12 = $textarea12;
}

if(isset($_POST['textarea13']))
{
$textarea13 =$_POST['textarea13'];
$email_textarea13 = $textarea13;
}

if(isset($_POST['textarea14']))
{
$textarea14 = $_POST['textarea14'];
$email_textarea14 = $textarea14;
}

if(isset($_POST['textarea15']))
{
$textarea15 = $_POST['textarea15'];
$email_textarea15 = $textarea15;
}

if(isset($_POST['textarea16']))
{
$textarea16 = $_POST['textarea16'];
$email_textarea16 = $textarea16;
}

if(isset($_POST['textarea17']))
{
$textarea17 = $_POST['textarea17'];
$email_textarea17 = $textarea17;
}

if(isset($_POST['textarea18']))
{
$textarea18 =$_POST['textarea18'];
$email_textarea18 = $textarea18;
}

if(isset($_POST['textarea19']))
{
$textarea19 =$_POST['textarea19'];
$email_textarea19 = $textarea19;
}

if(isset($_POST['textarea20']))
{
$textarea20 = $_POST['textarea20'];
$email_textarea20 = $textarea20;
}

if(isset($_POST['textarea21']))
{
$textarea21 = $_POST['textarea21'];
$email_textarea21 = $textarea21;
}

if(isset($_POST['textarea22']))
{
$textarea22 = $_POST['textarea22'];
$email_textarea22 = $textarea22;
}

if(isset($_POST['textarea23']))
{
$textarea23 = $_POST['textarea23'];
$email_textarea23 = $textarea23;
}

if(isset($_POST['textarea24']))
{
$textarea24 =$_POST['textarea24'];
$email_textarea24 = $textarea24;
}

if(isset($_POST['textarea25']))
{
$textarea25 = $_POST['textarea25'];
$email_textarea25 = $textarea25;
}

if(isset($_POST['textarea26']))
{
$textarea26 = $_POST['textarea26'];
$email_textarea26 = $textarea26;
}

if(isset($_POST['textarea27']))
{
$textarea27 =$_POST['textarea27'];
$email_textarea27 = $textarea27;
}

if(isset($_POST['textarea28']))
{
$textarea28 =$_POST['textarea28'];
$email_textarea28 = $textarea28;
}

if(isset($_POST['textarea29']))
{
$textarea29 = $_POST['textarea29'];
$email_textarea29 = $textarea29;
}

if(isset($_POST['textarea30']))
{
$textarea30 = $_POST['textarea30'];
$email_textarea30 = $textarea30;
}

if(isset($_POST['textarea31']))
{
$textarea31 = $_POST['textarea31'];
$email_textarea31 = $textarea31;
}


//The email body that will be visible by the recipient
$email_message = 
'<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <style>
        table {
          border-collapse:collapse;
        }
        td {
          font-size: 14px;
          font-family: "Arial";
          border-bottom: 1px dashed #e2e2e2;
          padding: 10px 0;
          line-height: 1.5em;
        }
      </style>
    </head>
    <body>
      <table width="600">
        <tr>
          <td width="30%"><strong>Name</strong></td>
          <td width="70%">'.$email_name.'</td>
        </tr>
        <tr>
          <td><strong>Email</strong></td>
          <td>'.$email_email.'</td>
        </tr>
        <tr>
          <td><strong>Website</strong></td>
          <td>'.$email_website.'</td>
        </tr>
        <tr>
          <td><strong>What your company does in one sentence?</strong></td>
          <td>'.$email_textarea1.'</td>
        </tr>
        <tr>
          <td><strong>What are your main differentiators and competitive advantages?</strong></td>
          <td>'.$email_textarea2.'</td>
        </tr>
        <tr>
          <td><strong>What are your main weaknesses?</strong></td>
          <td>'.$email_textarea3.'</td>
        </tr>
        <tr>
          <td><strong>Who are your main competitors and why?</strong></td>
          <td>'.$email_textarea4.'</td>
        </tr>
        <tr>
          <td><strong>Your Current Website</strong></td>
        </tr>
        <tr>
          <td><strong>What are the 3 things from your current website you would like to keep?</strong></td>
          <td>'.$email_textarea5.'</td>
        </tr>
        <tr>
          <td><strong>What would you definitely keep from your current design?</strong></td>
          <td>'.$email_textarea6.'</td>
        </tr>
        <tr>
          <td><strong>Would you like to add any specific functionality to your website?</strong></td>
          <td>'.$email_textarea7.'</td>
        </tr>
        <tr>
          <td><strong>Mention any specific ideas, wishes or expectations from the new design?</strong></td>
          <td>'.$email_textarea8.'</td>
        </tr>
        <tr>
          <td><strong>Your Customers</strong></td>
        </tr>
        <tr>
          <td><strong>Who are your customers? What target audience we should keep in mind during redesign?</strong></td>
          <td>'.$email_textarea9.'</td>
        </tr>
        <tr>
          <td><strong>Why people visit your website?</strong></td>
          <td>'.$email_textarea10.'</td>
        </tr>
        <tr>
          <td><strong>Are there certain things you say that your customers respond well to?</strong></td>
          <td>'.$email_textarea11.'</td>
        </tr>
        <tr>
          <td><strong>What strategy do you use to start communication with your potential customers?</strong></td>
          <td>'.$email_textarea12.'</td>
        </tr>
        <tr>
          <td><strong>Your Goals</strong></td>
        </tr>
        <tr>
          <td><strong>How would you define the success of the redesign?</strong></td>
          <td>'.$email_textarea13.'</td>
        </tr>
        <tr>
          <td><strong>More traffic, more leads? How do you get your leads? How many leads do you need to reach your revenue?</strong></td>
          <td>'.$email_textarea14.'</td>
        </tr>
        <tr>
          <td><strong>How ay visitors to the website you need to attract to reach the number of leads?</strong></td>
          <td>'.$email_textarea15.'</td>
        </tr>
        <tr>
          <td><strong>How would you define successful website? What are your expectations from the new design?</strong></td>
          <td>'.$email_textarea16.'</td>
        </tr>
        <tr>
          <td><strong>Your Marketing Strategy</strong></td>
        </tr>
        <tr>
          <td><strong>Are you sending people to your website from other marketing vehicles?</strong></td>
          <td>'.$email_textarea17.'</td>
        </tr>
        <tr>
          <td><strong>Do you send direct mail pieces or have other marketing collateral with "vanity URLs" that you need to keep when the new website goes live?</strong></td>
          <td>'.$email_textarea18.'</td>
        </tr>
        <tr>
          <td><strong>Do you have landing pages that will need to be redesigned on the new website?</strong></td>
          <td>'.$email_textarea19.'</td>
        </tr>
        <tr>
          <td><strong>What are the key performance indicators? What tools will you use to collect these measure?</strong></td>
          <td>'.$email_textarea20.'</td>
        </tr>
        <tr>
          <td><strong>What marketing software do you use to manage calls to action, landing pages, blogging, email and reporting?</strong></td>
          <td>'.$email_textarea21.'</td>
        </tr>
        <tr>
          <td><strong>Does your call-to-action convert visitors into leads and customers?</strong></td>
          <td>'.$email_textarea22.'</td>
        </tr>
        <tr>
          <td><strong>Your Design</strong></td>
        </tr>
        <tr>
          <td><strong>Do you like anything in particular about your current website?</strong></td>
          <td>'.$email_textarea23.'</td>
        </tr>
        <tr>
          <td><strong>What are some other websites you like (in your industry or others), and why?</strong></td>
          <td>'.$email_textarea24.'</td>
        </tr>
        <tr>
          <td><strong>Does your company have brand standards to adhere to?</strong></td>
          <td>'.$email_textarea25.'</td>
        </tr>
        <tr>
          <td><strong>What words would you use to describe the “look” you’d like to achieve for  your new website?</strong></td>
          <td>'.$email_textarea26.'</td>
        </tr>
        <tr>
          <td><strong>Your Content</strong></td>
        </tr>
        <tr>
          <td><strong>Will you update the website in-house, or will you need help? Do you have a specific content strategy?</strong></td>
          <td>'.$email_textarea27.'</td>
        </tr>
        <tr>
          <td><strong>Will you need a content management system so you can make changes in-house?</strong></td>
          <td>'.$email_textarea28.'</td>
        </tr>
        <tr>
          <td><strong>Are your product and service offers completely up-to-date?</strong></td>
          <td>'.$email_textarea29.'</td>
        </tr>
        <tr>
          <td><strong>Do your landing pages inspire people to learn more by digging deeper, or are they simply aesthetically pleasing pages that convey little value?</strong></td>
          <td>'.$email_textarea30.'</td>
        </tr>
        <tr>
          <td><strong>Is your site too text-heavy or riddled with corporate speak?</strong></td>
          <td>'.$email_textarea31.'</td>
        </tr>
      </table>
    </body>
    </html>
';

$email_to="deni@thenordicland.io";
$email_subject="Website Report for ".$email_website;
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=utf-8" . "\r\n";
$headers .= "CC: ".$email_cc;


@mail($email_to,$email_subject,$email_message,$headers);

header("Location:"."thankyou.html");

?>