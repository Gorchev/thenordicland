<?php

if(isset($_POST['service']))
{
$Service =$_POST['service'];
$email_Service = $Service;
}

if(isset($_POST['name']))
{
$Name =$_POST['name'];
$email_Name = $Name;
}

if(isset($_POST['email']))
{
$Email = $_POST['email'];
$email_Email = $Email;
}

if(isset($_POST['phone']))
{
$Telephone = $_POST['phone'];
$email_Telephone = $Telephone;
}

if(isset($_POST['movie']))
{
$Movie = $_POST['movie'];
$email_Movie = $Movie;
}

if(isset($_POST['message']))
{
$Message = $_POST['message'];
$email_Message = $Message;
}

//The email body that will be visible by the recipient
$email_message = 
'<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <style>
        table {
          border-collapse:collapse;
        }
        td {
          font-size: 14px;
          font-family: "Arial";
          border-bottom: 1px dashed #e2e2e2;
          padding: 10px 0;
          line-height: 1.5em;
        }
      </style>
    </head>
    <body>
      <table width="600">
        <tr>
          <td width="30%"><strong>About</strong></td>
          <td width="70%">'.$email_Service.'</td>
        </tr>
        <tr>
          <td><strong>From</strong></td>
          <td>'.$email_Name.'</td>
        </tr>
        <tr>
          <td><strong>Email</strong></td>
          <td>'.$email_Email.'</td>
        </tr>
        <tr>
          <td><strong>Phone</strong></td>
          <td>'.$email_Telephone.'</td>
        </tr>
        <tr>
          <td><strong>Website</strong></td>
          <td>'.$email_Movie.'</td>
        </tr>
        <tr>
          <td><strong>Message</strong></td>
          <td>'.$email_Message.'</td>
        </tr>
      </table>
    </body>
    </html>
';

$email_to="deni@thenordicland.io";
$email_subject="Enquiry for ".$email_Service;
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=utf-8" . "\r\n";
$headers .= "CC: ".$email_cc;


@mail($email_to,$email_subject,$email_message,$headers);

header("Location:"."thankyou.html");

?>