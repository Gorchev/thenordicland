$(document).ready(function(){

	//
	//
	// CONTACT US VALIDATION
	//
	//
	
	var name = $("#name");
	var email = $("#email");
	var phone = $("#phone");
	var message = $("#message");
	var button_validate = $("#validate");

	button_validate.click(function(){
		if( validateAbout() & validateName() & validateEmail() & validatePhone() & validateMessage()){
			return true;
		} else {
			return false;
		}
	}); 

	function validateAbout(){
		if($(".dropdown select option[value='1']").is(":selected")){
			$(".to_about").addClass("error");
			return false
		} else {
			$(".to_about").removeClass("error");
			return true
		}
	}

	function validateName(){
		if(name.val().length == 0){
			$(".to_name").addClass("error");
			return false
		} else {
			$(".to_name").removeClass("error");
			return true
		}
	}

	function validateEmail(){
		var a = $("#email").val();
		var regexp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;

		if(regexp.test(a)){
			$(".to_email").removeClass("error");
			return true
		} else {
			$(".to_email").addClass("error");
			return false
		}
	}

	function validatePhone(){
		var phoneNum = $("#phone").val();
		var regexp = /^[0-9-+]+$/;

		if(!regexp.test(phoneNum)){
			$(".to_phone").addClass("error");
			return false
		} else {
			$(".to_phone").removeClass("error");
			return true
		}
	}

	/* function validateAge(){
		var ageVer = $("#your_age").val();
		var regexp = /^[0-9-+]+$/;

		if(age.val() == "enter your age" || age.val() == " " || !regexp.test(ageVer)){
			age.addClass("error");
			$("#to_age").addClass("active");
			$("#to_age").text("Please enter your subject");
			return false
		} else {
			$("#to_age").removeClass("active");
			age.removeClass("error");
			return true
		}
	} */

	function validateMessage(){
		if(message.val().length == 0 ){
			$(".to_message").addClass("error");
			return false
		} else {
			$(".to_message").removeClass("error");
			return true
		}
	} 

	function validateCaptcha(){
		console.log(captcha.val())
		if(captcha.val() == ""){
			return true
		} else {
			return false
		}
	} 

});