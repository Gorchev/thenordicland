$(document).ready(function(){
	//Check the width of the window
	var winWidth = $(window).width();

	//Home Page - Parallax
	function onLoad() {
		window.onscroll = function() {
		    var speed = 5.0;
		    $(".about_me").css("background-position-y", (window.pageYOffset / speed - 400) + "px");
		}
	}

	onLoad();


	//Home Icons Hover 
	$(".services .service.web").hover(function(){
		$(".icon img",this).attr("src", "images/web_icon.gif");
	}, function(){
		$(".icon img",this).attr("src", "images/web_icon.png");
	});

	$(".services .service.dux").hover(function(){
		$(".icon img",this).attr("src", "images/dux_icon.gif");
	}, function(){
		$(".icon img",this).attr("src", "images/dux_icon.png");
	});

	$(".services .service.bi").hover(function(){
		$(".icon img",this).attr("src", "images/bi_icon.gif");
	}, function(){
		$(".icon img",this).attr("src", "images/bi_icon.png");
	});

	//Menu stick
	if(winWidth > 768) {
		$(window).on('scroll', function(){
			var fromTop = $(window).scrollTop();
			$("body").toggleClass("fixed-header", (fromTop > 100));
			if(fromTop > 100){
				//Change logo color to "white" when the position of the header is fixed
				//$(".site-header .logo img").attr("src", "images/logo_white.png");
			} else if ($("body").is("work_pagem, blog_inner_page")) {
				//Change logo color to "black" when the position of the header is NOT fixed
				//$(".site-header .logo img").attr("src", "images/logo_black.png");
			} 
			console.log(fromTop);
		});
	}

	//My work page and Blog inner pages - change logo to white
	//$(".work_page .site-header .logo img").attr("src", "images/logo_white.png");
	//$(".blog_inner_page .site-header .logo img").attr("src", "images/logo_white.png");

	//Contact Form Input Focus Animation
	$("form input, form textarea").on("focus", function(){
		$("form .info_block").each(function(){
			if($("input, textarea",this).val().length == 0){
				$(this).removeClass("active");
			} 
		});
		$(this).parents(".info_block").addClass("active");
	});

	//Mobile Menu - under mobile, change the orientation of the logo... change the logo itself
	if(winWidth < 1023) {
		$(".site-header .logo img").attr("src", "images/logo_white_mobile.png");
	}

	//Mobile Web Services Accordion - under mobile, create accordion for the web services page...
	$(".web_services .web_s").click(function(){
		$(".web_services .web_s p").slideUp(200);
		if($("p",this).is(":visible")){
			$(".web_services .web_s p").slideUp(200);
		} else {
			$("p",this).slideDown(200);	
		}
	});

	//Mobile Menu - toggle menu animation...
	$(".mobile_menu").click(function(){
		$(".site-header").toggleClass("active");
	});

	//Scroll to point
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 130
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
});

$(window).load(function(){
	$("#main").animate({opacity: 1});
});

